﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using OAuth2.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;

namespace OAuth2.Controllers
{
    public class FacebookController : Controller
    {
        private string appId = "2082285298464516";
        private string secret = "6d4b7fad8c3695100e32e7225d163b51";
        private string state = "TiSBK";
        private string redirectURI = "https://localhost:44307/Facebook/GetToken";
        private string baseURI = "https://graph.facebook.com/v2.11/";

        // GET: Facebook
        public ActionResult Login()
        {
            return Redirect($"https://www.facebook.com/v2.11/dialog/oauth?client_id={appId}&response_type=code&redirect_uri={redirectURI}&state={state}");
        }

        public ActionResult GetToken(string state, string code, string error_reason, string error, string error_description)
        {
            if (error == null)
            {
                HttpResponseMessage response;
                HttpClient client = new HttpClient() { BaseAddress = new Uri(baseURI + "oauth/") };

                response = client.GetAsync(client.BaseAddress + GetAccessToken(appId, redirectURI, secret, code)).Result;

                if (response.IsSuccessStatusCode)
                {
                    var jsonResponse = response.Content.ReadAsStringAsync().Result;
                    var tokenResponse = JsonConvert.DeserializeObject<TokenModel>(jsonResponse);

                    HttpContext.Session.Add("token", tokenResponse.Access_Token);
                    HttpContext.Session.Add("expires", tokenResponse.Expires_In);

                    return Redirect("https://localhost:44307/Facebook/ShowProfile");
                }
            }
            
            return Redirect("https://localhost:44307/Home/About");
        }

        private string GetAccessToken(string clientId, string redirectUri, string secret, string code)
        {
            return $"access_token?client_id={clientId}&redirect_uri={redirectUri}&client_secret={secret}&code={code}&scope=user_likes user_actions.news user_managed_groups pages_show_list";
        }

        public ActionResult ShowProfile()
        {
            string token = HttpContext.Session["token"].ToString();
            string results = "";
            
            HttpResponseMessage response;
            HttpClient client = new HttpClient() { BaseAddress = new Uri(baseURI) };

            response = client.GetAsync(client.BaseAddress + GetMyProfile(token)).Result;
            if (response.IsSuccessStatusCode)
            {
                var jsonResponse = response.Content.ReadAsStringAsync().Result;

                JObject deserializedResponse = JsonConvert.DeserializeObject(jsonResponse) as JObject;

                Dictionary<string, string> data = new Dictionary<string, string>();
                
                foreach (var item in deserializedResponse)
                {
                    data.Add(item.Key, item.Value.Value<string>());
                    ViewData[item.Key] = item.Value.Value<string>();
                }

                results += deserializedResponse.ToString().Trim();

                ViewData["Results"] = results;


                client = new HttpClient() { BaseAddress = new Uri(baseURI) };

                response = client.GetAsync(client.BaseAddress + GetMyPicture(token, data["id"])).Result;
                if (response.IsSuccessStatusCode)
                {
                    var jsonResponse2 = response.Content.ReadAsStringAsync().Result;

                    JObject deserializedResponse2 = JsonConvert.DeserializeObject(jsonResponse2) as JObject;

                    data.Add("photoUrl", deserializedResponse2["data"]["url"].ToString());
                    ViewData["photoUrl"] = deserializedResponse2["data"]["url"].ToString();

                    client = new HttpClient() { BaseAddress = new Uri(baseURI) };

                    response = client.GetAsync(client.BaseAddress + GetMyCover(token, data["id"])).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var jsonResponse3 = response.Content.ReadAsStringAsync().Result;

                        JObject deserializedResponse3 = JsonConvert.DeserializeObject(jsonResponse3) as JObject;

                        data.Add("cover", deserializedResponse3["cover"]["source"].ToString());
                        ViewData["cover"] = deserializedResponse3["cover"]["source"].ToString();

                        return View();
                    }

                    return Redirect("https://localhost:44307/Home/About");
                }

                return Redirect("https://localhost:44307/Home/About");
            }

            return Redirect("https://localhost:44307/Home/About");
        }

        private static string GetMyProfile(string token)
        {
            return $"me?access_token={token}";
        }

        private static string GetMyPicture(string token, string id)
        {
            return $"{id}/picture?redirect=false&type=large&access_token={token}";
        }

        private static string GetMyCover(string token, string id)
        {
            return $"{id}?fields=cover&access_token={token}";
        }
    }
}