﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OAuth2.Models
{
    public class TokenModel
    {
        public string Token_Type { get; set; }
        public string Access_Token { get; set; }
        public int Expires_In { get; set; }
    }
}